// peer2peer.c - a decentralized / distributed multi-person chat application

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define STDIN 0
#define PORT 2048
#define N_PEERS 5
#define QUE_LEN 10
#define BUF_LEN 256
#define IP_LEN 30
#define MSG_LEN 200
#define STDIN_LEN 230
#define UNAME_LEN 30

static char **userNames;
static char **ipAddrs;
static int userCount;

void exit_err(char *msg) {
    perror(msg);
    exit(0);
}

int getLines (char *fPath) {
    FILE* fp = fopen(fPath, "r");
    char ch;
    int lines = 0;
    while(!feof(fp)) {
        ch = (char) fgetc(fp);
        if(ch == '\n') {
            lines++;
        }
    }
    return lines;
}

void loadUsers (char *csvFile) {
    char line[BUF_LEN];
    int iter, len;
    userCount = getLines(csvFile);
    userNames = (char **) malloc(userCount * sizeof(char*));
    ipAddrs = (char **) malloc(userCount * sizeof(char*));
    for(iter = 0; iter < userCount; iter ++) {
        userNames [iter] = (char *) malloc(UNAME_LEN * sizeof(char));
        ipAddrs [iter] = (char *) malloc(IP_LEN * sizeof(char));
    }
    iter = 0;
    FILE* fp = fopen(csvFile, "r");
    if (fp == NULL)
        exit_err("Could not open users' file\n");
    while (fgets(line, BUF_LEN, fp) != NULL) {
        len = (int) strlen(line);
        if (line[len - 1] == '\n')
            line[len - 1] = '\0';
        if (strlen(line) == 0)
            continue;
        strcpy(userNames [iter], strtok(line, ","));
        strcpy(ipAddrs [iter], strtok(NULL, ","));
        iter ++;
    }
}

char *get_ipAddr (char *userName) {
    int iter;
    for (iter = 0; iter < userCount; iter ++)
        if (strcmp(userName, userNames [iter]) == 0)
            break;
    if (iter >= userCount)
        return NULL;
    return ipAddrs [iter];
}

char *get_userName (char *ipAddr) {
    int iter;
    for (iter = 0; iter < userCount; iter ++)
        if (strcmp(ipAddr, ipAddrs [iter]) == 0)
            break;
    if (iter >= userCount)
        return NULL;
    return userNames [iter];
}

int main() {
    char *slash_pos, *ipAddr;
    int iter, iter2;
    struct timeval wait_time;
    wait_time.tv_sec = 0;
    wait_time.tv_usec = 500000;
    fd_set master;    // master file descriptor list
    char std_in_buf[STDIN_LEN];
    char user_name[IP_LEN];
    char message[MSG_LEN];
    fd_set read_fds; // temp file descriptor list for select()
    struct hostent *he;
    char buf[BUF_LEN];

    struct sockaddr_in my_address;     // server address
    struct sockaddr_in remote_address; // client address
    struct sockaddr_in their_address; // client address
    int max_fd;    // maximum file descriptor number
    int listener;    // listening socket descriptor
    int sender[N_PEERS];
    int sender_count = 0;
    int newfd;    // newly accept()ed socket descriptor

    int num_bytes;    // buffer for client data
    int yes = 1;
    int address_len;

    struct sockaddr_in address;
    int len;
    char *ip;

    loadUsers("users.csv");
    // clear the master and temp sets

    FD_ZERO(&master);
    FD_ZERO(&read_fds);

    // Create the central listener socket
    if ((listener = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        exit_err("Could not create socket");

    // Create the decentralised sender sockets
    for (iter = 0; iter < N_PEERS; iter++) {
        if ((sender[iter] = socket(AF_INET, SOCK_STREAM, 0)) == -1)
            exit_err("Could not create socket");
    }

    // Reuse port just after connection ends
    if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        exit_err("Could not call setsockopt");

    // bind listener to port
    my_address.sin_family = AF_INET;
    my_address.sin_addr.s_addr = INADDR_ANY;
    my_address.sin_port = htons(PORT);
    memset(&(my_address.sin_zero), 0, 8);
    if (bind(listener, (struct sockaddr *) &my_address, sizeof(my_address)) == -1)
        exit_err("bind");

    // listen to the listener socket
    if (listen(listener, QUE_LEN) == -1)
        exit_err("listen");

    // add the listener and STDIN to the master set
    FD_SET(listener, &master);
    FD_SET(STDIN, &master);

    // store the largest file descriptor
    max_fd = (STDIN > listener) ? STDIN : listener;

    // Interconnect peers
    while (1) {
        read_fds = master; // copy it
        if (select(max_fd + 1, &read_fds, NULL, NULL, &wait_time) == -1)
            exit_err("Error in select");

        // search for data to read from existing connections
        for (iter = 0; iter <= max_fd; iter++) {
            if (FD_ISSET(iter, &read_fds)) { // we got one!!

                // The connection is from server socket
                if (iter == listener) {

                    // handle new connections
                    address_len = sizeof(remote_address);

                    if ((newfd = accept(listener, (struct sockaddr *) &remote_address, (socklen_t *) &address_len)) == -1)
                        perror("Couldn't accept new connection");

                    else {
                        FD_SET(newfd, &master); // add to master set
                        if (newfd > max_fd)
                            max_fd = newfd;
                        ipAddr = inet_ntoa(remote_address.sin_addr);
                        printf("%s (%s) connected with you on socket %d\n", get_userName(ipAddr), ipAddr, newfd);
                    }
                } else if (iter == STDIN) {  // Connection is from STDIN

                    // Clear string, name, message storage buffers
                    bzero(std_in_buf, STDIN_LEN);
                    bzero(user_name, IP_LEN);
                    bzero(message, MSG_LEN);

                    scanf(" %[^\n]*c", std_in_buf);
                    slash_pos = strchr(std_in_buf, '/');
                    strcpy(user_name, std_in_buf);
                    user_name[slash_pos - std_in_buf] = 0;
                    strcpy(message, slash_pos + 1);
                    len = sizeof(address);
                    int no_client_connection = 1;
                    for (iter2 = 0; iter2 < sender_count; iter2++) {
                        if (getpeername(sender[iter2], (struct sockaddr *) &address, (socklen_t *) &len) != -1) {
                            ip = inet_ntoa(address.sin_addr);
                            if (strcmp(ip, get_ipAddr(user_name)) == 0) {
                                no_client_connection = 0;
                                send(sender[iter2], message, strlen(message), 0);
                            }
                        }
                    }
                    if (no_client_connection) {
                        if (sender_count == N_PEERS)
                            exit_err("Connection limit exceeded");
                        if ((he = gethostbyname(get_ipAddr(user_name))) == NULL)
                            exit_err("Get host by name failed");

                        // Get ip info of client
                        their_address.sin_family = AF_INET;
                        their_address.sin_port = htons(PORT);
                        their_address.sin_addr = *((struct in_addr *) he->h_addr);
                        bzero(&(their_address.sin_zero), 8);

                        // Create new connection to client
                        if (connect(sender[sender_count], (struct sockaddr *) &their_address, sizeof(struct sockaddr)) ==
                            -1)
                            exit_err("Couldn't connect with sender");

                        getpeername(sender[sender_count], (struct sockaddr *) &address, (socklen_t *) &len);
                        send(sender[sender_count], message, strlen(message), 0);

                        // 1 More sender now
                        sender_count++;
                    }
                } else {
                    // handle data from a client
                    bzero(buf, BUF_LEN);
                    if ((num_bytes = (int) recv(iter, buf, sizeof(buf), 0)) <= 0) {
                        // got error or connection closed by client
                        if (num_bytes == 0) // connection closed
                            printf("Socket %d has hung up\n", iter);
                        else // Connection hung up unexpectedly
                            perror("The socket has hung up unexpectedly");
                        close(iter); // close this file descriptor
                        FD_CLR(iter, &master); // remove from master set
                        sender_count--;
                    } else {
                        // Read the message and display
                        len = sizeof(address);
                        if (getpeername(iter, (struct sockaddr *) &address, (socklen_t *) &len) != -1) {
                            // Successfully read
                            ip = inet_ntoa(address.sin_addr);
                            printf("New message from %s (%s) => %s\n", get_userName(ip), ip, buf);
                        } else {
                            // Read unsuccessful
                            exit_err("Read unsuccesful");
                            exit(0);
                        }
                        bzero(buf, BUF_LEN);
                    }
                }
            }
        }
    }
    return 0;
}